<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question1;

class Answer extends Model
{
    protected $guarded = [];
    public function question1()
    {
      return $this->belongsTo('App\Question1');
    }
}

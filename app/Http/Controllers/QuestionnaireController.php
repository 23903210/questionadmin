<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\Questionnaires;
use App\Question1;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // get all the questionnaires
      $questionnaires = questionnaire::all();

      return view('questionnaires', ['questionnaires' => $questionnaires]);
      //return view('admin/questionnaire/questionnaireadmin', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //$ques = Question1::pluck('questiontitle', 'id');

        // now we can return the data with the view
        return view('admin/questionnaire/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd(request()->all());
      $this->validate($request, [
          'title' => 'required',
          'detail' => 'required',
          'researcher' => 'required',
      ]);
      $questionnaire = Questionnaire::create($request->all());

      return redirect('questionnaires/'.$questionnaire->id);
      //return redirect('admin/questionnaires');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $questionnaire = Questionnaire::where('id',$id)->first();

  // if article does not exist return to list
    if(!$questionnaire)
    {
      return redirect('/admin/questionnaires'); // you could add on here the flash messaging of article does not exist.
    }
    return view('/admin/questionnaire/show')->withQuestionnaire($questionnaire);

}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

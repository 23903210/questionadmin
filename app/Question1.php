<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Questionnaire;
use App\Answer;
class Question1 extends Model
{
  protected $fillable = [
    'questiontitle',
  ];
  public function questionnaires()
  {
      return $this->belongsTo('App\Questionnaire');
  }

  public function answers()
  {
    return $this->hasMany('App\Answer');
  }
}

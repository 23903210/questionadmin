<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question1;

class Questionnaire extends Model
{
  protected $fillable = [
      'title',
      'detail',
      'researcher',

  ];

  /**
   * Get the categories associated with the given article.
   *
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function question1s()
  {
      return $this->hasMany('App\Question1');
  }

  /**
   * Get the user associated with the given article
   *
   * @return mixed
   */
  public function user()
  {
      return $this->belongsTo('App\User');
  }
}

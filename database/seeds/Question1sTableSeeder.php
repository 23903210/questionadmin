<?php

use Illuminate\Database\Seeder;

class Question1sTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('question1s')->insert([
          ['id' => 1, 'questiontitle' => "what is your name"],
          ['id' => 2, 'questiontitle' => "what is your  friends name"],
          ['id' => 3, 'questiontitle' => "what do you study"],

        ]);
    }
}

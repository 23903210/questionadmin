<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create question</title>
</head>
<body>
<h1>Add Question</h1>

<form action="/questionnaires/{{ $questionnaire->id }}" action ='QuestionnaireController@store' method="post">

  <div>
    <label for="questiontitle">Question Title:</label>
    <input name="question1[questiontitle]" type="text" id="questiontitle">
  </div>

  <div>
    <fieldset>
      <legend> Choices</legend>
      <div>
        <div>
          <label for="answer1">Answer 1:</label>
          <input name="answers[][answer1]" type="text" id="answer1">
        </div>
      </div>
      <div>
        <div>
          <label for="answer2">Answer 2:</label>
          <input name="answers[][answer1]" type="text" id="answer2">
        </div>
      </div>
      <div>
        <div>
          <label for="answer3">Answer 3:</label>
          <input name="answers[][answer1]" type="text" id="answer3">
        </div>
      </div>
      <div>
        <div>
          <label for="answer4">Answer 4:</label>
          <input name="answers[][answer1]" type="text" id="answer4">
        </div>
      </div>

  </fieldset>
  </div>

  <button type="submit">Create Question</button>
  </form>

</body>
</html>

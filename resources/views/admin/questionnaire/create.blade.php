<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create questionnaire</title>
</head>
<body>
<h1>Add Questionnaire</h1>
@if ($errors->any())
      <div>
          <ul class="alert alert-danger">
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
{!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!}

    <div class="row large-12 columns">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('detail', 'Detail:') !!}
        {!! Form::textarea('detail', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('researcher', 'Researcher:') !!}
        {!! Form::text('researcher', null, ['class' => 'large-8 columns']) !!}
    </div>




    <div class="row large-4 columns">
        {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}

</body>
</html>

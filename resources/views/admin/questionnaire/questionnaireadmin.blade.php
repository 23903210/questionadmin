<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Questionnaires</title>
</head>
<body>
  <header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
         <div class="container-fluid">
              <ul class="nav navbar-nav">
                 <a href="/questionnaires">Questionnaires</a>

                 <a href="/welcome">Informed Consent</a>
             </ul>
         </div>
     </nav>
  </header>
<h1>Questionnaires</h1>
<section>
    @if (isset ($questionnaires))

        <ul>
            @foreach ($questionnaires as $questionnaire)
                <li><a href="/admin/questionnaires/questionnaireadmin/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li>
            @endforeach
        </ul>
    @else
        <p> no questionnaires added yet </p>
    @endif
</section>

{{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
    <div class="row">
        {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}
</body>
</html>

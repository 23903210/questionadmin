
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $questionnaire->title }}</title>
</head>
<body>
  <h1>Question Title:</h1>
<h1>{{ $questionnaire->title }}</h1>
<p>Detail: {{ $questionnaire->detail }}</p>
<p>Researcher: {{ $questionnaire->researcher }}</p>
<section>
    @if (isset ($question1s))

        <ul>
            @foreach ($question1s as $question1)
                <li><a href="/admin/questionnaires/questionnaireadmin/{{ $questionnaire->id }}/question1/create" name="{{ $question1->questiontitle }}">{{ $questionnaire->title }}</a></li>
            @endforeach
        </ul>
    @else
        <p> no questions added yet </p>
    @endif
</section>

<a href="/admin/questionnaires/{{ $questionnaire->id }}/question1/create">Add new Question</a>
</body>
</body>
</html>

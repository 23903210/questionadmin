<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Questionnaires</title>
</head>
<body>
  <header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
         <div class="container-fluid">
              <ul class="nav navbar-nav">
                 <a href="/questionnaires">Questionnaires</a>

                 <a href="/welcome">Informed Consent</a>
             </ul>
         </div>
     </nav>
  </header>
<h1>Questionnaires</h1>
<section>
    @if (isset ($questionnaires))

        <ul>
            @foreach ($questionnaires as $questionnaire)
                <li><a href="/questionnaires/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li>
            @endforeach
        </ul>
    @else
        <p> no questionnaires added yet </p>
    @endif
</section>
<footer>
  <ul>
    <p>  Facebook</p>
    <p> Twitter</p>
    <p> Email: questionnaires@gmail.com </p>
    <p> Telephone: 0161 2340  </p>
  </ul>
</footer>

</body>
</html>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Informed Consent</title>
</head>
<body>
  <header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
         <div class="container-fluid">
              <ul class="nav navbar-nav">
                 <a href="/questionnaires">Questionnaires</a>

                 <a href="/welcome">Informed Consent</a>
             </ul>
         </div>
     </nav>
  </header>
<h1>Informed Consent</h1>
<section>
    <p>By reading this page, you have decided to participate in one of our questionnaires and know that there will be anonymity and confidentiality if you wish to proceed.
    If you do not finsh the questionnaire then your data will not be used for research. Once you have completed the questionnaire, the researcher is able to use your data in any way they wish.
    Next to the questionnaire, there will be details provided on what they are about  and their purpose. Thank you for taking time to read this.</p>
</section>
<footer>
  <ul>
    <p>  Facebook</p>
    <p> Twitter</p>
    <p> Email: questionnaires@gmail.com </p>
    <p> Telephone: 0161 2340  </p>
  </ul>
</footer>
</body>
</html>

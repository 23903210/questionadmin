<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('/questionnaires', 'QuestionnaireController' );
Route::resource('/admin/questionnaire/questionnaireadmin', 'QuestionnaireController' );
//Route::resource('/admin/questionnaires/{questionnaire}/question1/create', 'Question1Controller');
Route::get('/', function () {
    return view('/questionnaires');
});
Route::get('/admin/questionnaires/questionnaireadmin', function () {
    return view('/admin/questionnaire/questionnaireadmin');
});
Route::get('/welcome', function () {
    return view('/welcome');
});
//Route::resource('/admin/questionnaires/.$questionnaire->id/question1/create', 'Question1Controller@create');

Route::get('/admin/questionnaires/{questionnaire}/question1/create', 'Question1Controller@create');
Route::post('/admin/questionnaires/{questionnaire}', 'Question1Controller@store');
//Route::get('/questionnaire/create', function () {
//    return view('/question/create');
//});



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

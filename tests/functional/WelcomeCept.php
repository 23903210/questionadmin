<?php
$I = new FunctionalTester($scenario);
$I->am('an admin');
$I->wantTo('test laravel working');
//when
$I->amOnPage('/');

//then
$I->seeCurrentUrlEquals('/');
$I->See('Laravel', '.title');
